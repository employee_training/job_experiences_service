CREATE DATABASE  IF NOT EXISTS `employee_training` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `employee_training`;
-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: employee_training
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `form_job_experiences`
--

DROP TABLE IF EXISTS `form_job_experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `form_job_experiences` (
  `id` int NOT NULL AUTO_INCREMENT,
  `form_user_id` int NOT NULL,
  `job_name` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `start_year` year NOT NULL,
  `end_year` year DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `form_job_experiences_FK` (`form_user_id`),
  KEY `form_job_experiences_id_IDX` (`id`) USING BTREE,
  KEY `form_job_experiences_FK_1` (`user_id`),
  CONSTRAINT `form_job_experiences_FK` FOREIGN KEY (`form_user_id`) REFERENCES `form_user_register` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `form_job_experiences_FK_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_job_experiences`
--

LOCK TABLES `form_job_experiences` WRITE;
/*!40000 ALTER TABLE `form_job_experiences` DISABLE KEYS */;
INSERT INTO `form_job_experiences` VALUES (7,4,'Data Analysts','Shopee',2018,2019,4),(10,5,'Backend Programmer','Tokopedia',2018,2021,6),(11,5,'Backend Programmer','Bukalapak',2019,2022,6),(13,5,'Backend Programmer','Bukalapak',2019,2022,4);
/*!40000 ALTER TABLE `form_job_experiences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_user_register`
--

DROP TABLE IF EXISTS `form_user_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `form_user_register` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `address` varchar(255) NOT NULL,
  `division` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `year_submission` year NOT NULL,
  PRIMARY KEY (`id`),
  KEY `form_user_regiser_id_IDX` (`id`) USING BTREE,
  KEY `form_user_register_FK` (`user_id`),
  CONSTRAINT `form_user_register_FK` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_user_register`
--

LOCK TABLES `form_user_register` WRITE;
/*!40000 ALTER TABLE `form_user_register` DISABLE KEYS */;
INSERT INTO `form_user_register` VALUES (4,4,'Morioh Cho','IT','Programmer',2020),(5,6,'Morioh Cho','IT','Programmer',2020);
/*!40000 ALTER TABLE `form_user_register` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_salt` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `role` enum('admin','user') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UN` (`email`),
  UNIQUE KEY `users_UN` (`username`),
  KEY `users_id_IDX` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (4,'Rahadian Arya Prasetya Wiratama','rahadian271094@gmail.com','434f734f50317689d898490d0e665c66091a4e3f97ffc395d503d34e274edb61','95afdec08636a55f5b339cc4d567ca9b','aryatux1337','admin'),(6,'Jotaro Kujo','jojo@jojo','c48b45055e3a73d4d2fa2fa79ab05661bb2a784dcb801cae74e25a2adb7ed24b','018ef5a4c3ebc3d891f82c98146587c1','jojo','user');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-08 11:30:03
