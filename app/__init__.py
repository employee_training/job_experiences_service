from flask import Flask
from config import Config
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config.from_object(Config)
jwt = JWTManager(app)

from app.routers.usersRouter import *
from app.routers.form_jobRouter import *

app.register_blueprint(users_blueprint)
app.register_blueprint(form_job_blueprint)