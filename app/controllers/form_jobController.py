from app.models.form_jobModel import Form_Job, Form_User, Session, engine, Base
from app.models.usersModel import Users
from flask import jsonify, request
from flask_jwt_extended import *
import os,json,datetime
from hashlib import pbkdf2_hmac

Base.metadata.create_all(engine)
session = Session()


# show users join with form users data
@jwt_required()
def showJoinJobForm():
    user_id = get_jwt_identity()
    res_list = []
    final_res = []
    data_dict = {}
    
    if (user_id[0]['role']=='admin'): 
        data_dict['code'] = 200
        data_dict['success'] = True
        for u,fu,fj in session.query(Users, Form_User, Form_Job).join(Form_User, Form_User.user_id == Users.id).join(Form_Job, Form_Job.user_id == Users.id).order_by(Form_User.user_id.asc()).all():
            col = ['user_id','fullname','email','username','division','position','year_submission','job_name','company','start_year','end_year']
            val = fu.user_id,u.fullname,u.email,u.username,fu.division,fu.position,fu.year_submission,fj.job_name,fj.company,fj.start_year,fj.end_year
            res = dict(zip(col,val))
            res_list.append(res)
        data_dict['data'] = res_list
        data_dict['message'] = "Data found"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 200
        return result
    else:
        data_dict['code'] = 200
        data_dict['success'] = True
        for u,fu,fj in session.query(Users, Form_User, Form_Job).join(Form_User, Form_User.user_id == Users.id).join(Form_Job, Form_Job.user_id == Users.id).filter(Form_User.user_id == '{}'.format(user_id[0]['id'])).order_by(Form_User.user_id.asc()):
            col = ['user_id','fullname','email','username','division','position','year_submission','job_name','company','start_year','end_year']
            val = fu.user_id,u.fullname,u.email,u.username,fu.division,fu.position,fu.year_submission,fj.job_name,fj.company,fj.start_year,fj.end_year
            res = dict(zip(col,val))
            res_list.append(res)
        data_dict['data'] = res_list
        data_dict['message'] = "Data found"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 200
        return result
   
   
# show all form job experiences
@jwt_required()
def showFormJob():
    user_id = get_jwt_identity()
    res_list = []
    final_res = []
    data_dict = {}
    if (user_id[0]['role']=='admin'): 
        data = session.query(Form_Job).all()
        try:
            data_dict['code'] = 200
            data_dict['success'] = True
            for row in data:
                col = ['id','form_user_id','job_name','company','start_year','end_year']
                val = row.id,row.form_user_id,row.job_name,row.company,row.start_year,row.end_year
                res = dict(zip(col,val))
                res_list.append(res)
            data_dict['data'] = res_list
            data_dict['message'] = "Data found"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result
        except:
            data_dict['code'] = 401
            data_dict['success'] = False
            data_dict['message'] = "Data error"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 401
            return result
    else:
        data_dict['code'] = 403
        data_dict['success'] = False
        data_dict['message'] = "Permission not allowed"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 403
        return result

#insert new form job experiences
@jwt_required()
def insertFormJob(**params):
    user_id = get_jwt_identity()
    final_res = []
    data_dict = {}
    try:

        data_dict['code'] = 200
        data_dict['success'] = True
        data = Form_Job(params['form_user_id'],user_id[0]['id'],params['job_name'],params['company'],params['start_year'],params['end_year'])
        session.add(data)
        session.commit()
        data_dict['message'] = "Form Job created successfully"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 200
        return result
    except:
        data_dict['code'] = 400
        data_dict['success'] = False
        data_dict['message'] = "Create Form Job failed"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 403
        return result

#update data form job experiences
@jwt_required()
def updateFormJobById(userid,**params):
    user_id = get_jwt_identity()
    final_res = []
    data_dict = {}
    data = session.query(Form_Job).filter(Form_Job.user_id == '{}'.format(userid),Form_Job.id == '{}'.format(params['id'])).first()
    if (user_id[0]['role']=='admin'):    
        if params['job_name'] and params['company'] and params['start_year'] and params['end_year']:
            data_dict['code'] = 200
            data_dict['success'] = True
            data.job_name = params['job_name']
            data.company = params['company']
            data.start_year = params['start_year']
            data.end_year = params['end_year']
            if data.job_name:
                session.commit()
                data_dict['message'] = "Job experience updated successfully"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 200
                return result
            else:
                data_dict['code'] = 200
                data_dict['success'] = True
                data_dict['message'] = "Data not found"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 200
                return result

        else:
            data_dict['code'] = 400
            data_dict['success'] = False
            data_dict['message'] = "All datas must be filled"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 400
        return result
    else:
        if userid == user_id[0]['id']:
            if params['job_name'] and params['company'] and params['start_year'] and params['end_year']:
                data_dict['code'] = 200
                data_dict['success'] = True
                data.job_name = params['job_name']
                data.company = params['company']
                data.start_year = params['start_year']
                data.end_year = params['end_year']
                if data.job_name:
                    session.commit()
                    data_dict['message'] = "Job experience updated successfully"
                    final_res.append(data_dict)
                    result = jsonify(final_res)
                    result.status_code = 200
                    return result
                else:
                    data_dict['code'] = 200
                    data_dict['success'] = True
                    data_dict['message'] = "Data not found"
                    final_res.append(data_dict)
                    result = jsonify(final_res)
                    result.status_code = 200
                    return result
            else:
                data_dict['code'] = 400
                data_dict['success'] = False
                data_dict['message'] = "All datas must be filled"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 400
                return result

        else:
            data_dict['code'] = 401
            data_dict['success'] = False
            data_dict['message'] = "You can only edit your own data"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 401
            return result


# #delete data form job experiences by id
@jwt_required()
def deleteFormJobById(userid,**params):
    user_id = get_jwt_identity()
    final_res = []
    data_dict = {}
    data = session.query(Form_Job).filter(Form_Job.user_id == '{}'.format(userid),Form_Job.id == '{}'.format(params['id'])).first()
    try:
        if (user_id[0]['role']=='admin'):  
            session.delete(data)
            session.commit()
            data_dict['message'] = "Job experience deleted successfully"
            final_res.append(data_dict)
            result = jsonify(final_res)
            result.status_code = 200
            return result

        else:
            if userid == user_id[0]['id']:
                session.delete(data)
                session.commit()
                data_dict['message'] = "Job experience deleted successfully"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 200
                return result
            else:
                data_dict['code'] = 401
                data_dict['success'] = False
                data_dict['message'] = "You can only delete your own data"
                final_res.append(data_dict)
                result = jsonify(final_res)
                result.status_code = 401
                return result
    except:
        data_dict['code'] = 400
        data_dict['success'] = False
        data_dict['message'] = "Failed"
        final_res.append(data_dict)
        result = jsonify(final_res)
        result.status_code = 400
        return result







   