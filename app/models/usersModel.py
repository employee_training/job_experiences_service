from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from flask import request


engine = create_engine('mysql+mysqlconnector://root:motherrussia@localhost:3306/employee_training', echo = True)
Session = sessionmaker(bind=engine)
Base = declarative_base()

class Users(Base):
    __tablename__ = 'users'
    id = Column(Integer,primary_key=True)
    fullname = Column(String)
    email = Column(String)
    password_hash = Column(String)
    password_salt = Column(String)
    username = Column(String)
    role = Column(String)

    def __init__(self,fullname:str,email:str,password_hash:str,password_salt:str,username:str,role:str):
        self.fullname = fullname
        self.email = email
        self.password_hash = password_hash
        self.password_salt = password_salt
        self.username = username
        self.role = role