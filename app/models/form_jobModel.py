from sqlalchemy import Column, ForeignKey, Integer, String
from app.models.usersModel import Users
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, aliased
from sqlalchemy.ext.declarative import declarative_base
from flask import request
import sys



engine = create_engine('mysql+mysqlconnector://root:motherrussia@localhost:3306/employee_training', echo = True)
Session = sessionmaker(bind=engine)
Base = declarative_base()

engine.has_table("form_user_register")

class Form_User(Base):
    __tablename__ = 'form_user_register'
    id = Column(Integer,primary_key=True)
    user_id = Column(Integer, ForeignKey(Users.id))
    address = Column(String)
    division = Column(String)
    position = Column(String)
    year_submission = Column(Integer)

class Form_Job(Base):
    __tablename__ = 'form_job_experiences'
    id = Column(Integer,primary_key=True)
    form_user_id = Column(Integer, ForeignKey('form_user_register.id'))
    user_id = Column(Integer, ForeignKey(Users.id))
    job_name = Column(String)
    company = Column(String)
    start_year = Column(Integer)
    end_year = Column(Integer)

    def __init__(self,form_user_id:int,user_id:int,job_name:str,company:str,start_year:int,end_year:int):
        self.form_user_id = form_user_id
        self.user_id = user_id
        self.job_name = job_name
        self.company = company
        self.start_year = start_year
        self.end_year = end_year
        

