from app import app
from app.controllers import usersController
from flask import Blueprint, request

users_blueprint = Blueprint("usersRouter",__name__)

@app.route("/users",methods=["GET"])
def showUsers():
    return usersController.showUsers()

@app.route("/user/<int:id>",methods=["GET"])
def showUserbyId(id):
    return usersController.showUserbyId(id)

@app.route("/user/create",methods=["POST"])
def insertUser():
    params = request.json
    return usersController.insertUser(**params)

@app.route("/user/login",methods=["POST"])
def userLogin():
    params = request.json
    return usersController.userLogin(**params)

@app.route("/user/update/<int:id>",methods=["POST"])
def updateUserById(id):
    params = request.json
    return usersController.updateUserById(id,**params)

@app.route('/user/delete/<int:id>',methods=['DELETE'])
def deleteUserById(id):
    return usersController.deleteUserById(id)